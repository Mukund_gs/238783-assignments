package movietickets;

import java.io.*;
import java.util.*;
public class Movietix 
{

	public static void main(String[] args) 
	{
		Scanner sc =new Scanner(System.in);
		int ticket;
		double total=0.0,disc=0.0;
		char drink,coup,circ;
		System.out.println("Enter the no of ticket:");
		ticket=sc.nextInt();
		if(ticket<5||ticket>40)
		{
			System.out.println("Minimum of 5 and Max of 40");
			System.exit(0);
		}
		System.out.println("Do you want a Refreshment:");
		drink=sc.next().charAt(0);
		System.out.println("Do you have a Coupon ?:");
		coup=sc.next().charAt(0);
		System.out.println("Give the Circle you want :");
		circ=sc.next().charAt(0);
		
			if(circ == 'k')
			{
				total= total + (ticket*75);
			}
			else if(circ == 'q')
			{
				total= total + (ticket*150);
			}
			else
			{
				System.out.println("Invalid Input");
				System.exit(0);
			}
			if(ticket>20)
			{
				disc=(total*10)/100;
				total= total- disc;
			}
			if(coup=='y'||coup=='Y')
			{
				disc=(total*2)/100;
				total= total-disc;
			}
			if(drink=='y'||drink=='Y')
			{
				total= total+(ticket*50);
			}
			System.out.printf("Ticket Price is %.2f",total);
	}

}
