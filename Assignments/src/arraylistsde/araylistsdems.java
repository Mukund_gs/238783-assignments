package arraylistsde;
import java.util.ArrayList;
import java.util.Iterator;

public class araylistsdems 
{
    public static void main(String[] args)
    {
        ArrayList<String> list = new ArrayList<String>();
        list.add("10");
        list.add("20");
        list.add("30");
        System.out.println(list.size());
        System.out.println("While Loop result:");
        
        
        Iterator<String> itr = list.iterator();
        while(itr.hasNext())
        {
            System.out.println(itr.next());
        }
        
        
        System.out.println("Its Advanced Loop:");
        for(Object obj : list)
        {
            System.out.println(obj);
        }
        
        System.out.println("For Loop result:");
        for(int i=0; i<list.size(); i++)
        {
            
            System.out.println(list.get(i));
        }
    }
}


