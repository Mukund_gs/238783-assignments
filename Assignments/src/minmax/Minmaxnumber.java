package minmax;
import java.io.*;
import java.util.*;

public class Minmaxnumber 
{

	public static void main(String[] args) 
	{	
		System.out.println("Enter the 5 numbers:");
		Scanner sc =new Scanner(System.in);
		Long arr[]= new Long[5];
		int sum = 0;
		for(int i=0;i<5;i++)
		{
			arr[i]=sc.nextLong();
			sum += arr[i];
		}
		Arrays.sort(arr);
		Long min=arr[4];
		System.out.println("The Max Value in the Array is :" +min);
		Long max=arr[0];
		System.out.println("The Min Value in the Array is :" +max);
		Long minSum= sum-min;
		Long maxSum= sum-max;
		System.out.println("The Max value and the Min value sum is");
		System.out.println("MinSum      MaxSum");
		System.out.println(minSum+ "             "+ maxSum);
	}
}
